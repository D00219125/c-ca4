#pragma once
#include "Bug.h"

class Hopper : public Bug
{
	int hopLength;
public:
	virtual void move();
	Hopper(int id, pair<int, int> position, int direction, int size, int hopLength);
	friend ostream& operator<<(ostream& strm, const Hopper& hop);
};