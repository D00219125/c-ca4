#include "Bug.h"

void Bug::move()
{/*
	//rotation
	while (isWayBlocked())
	{
		randomDirection();
	}
	//movement
	switch (this->direction)
	{
	case 1://north
		cout << "previous pos = " << this->position.first << "," << this->position.second << endl;
		this->position.first -= 1;
		cout << "new pos = " << this->position.first << "," << this->position.second << endl;
		if (this->position.first < 0)//these wont be reached with crawlers if I did the rotation right
		{
			this->position.first = 0;
		}
		break;
	case 2://east
		cout << "previous pos = " << this->position.first << "," << this->position.second << endl;
		this->position.second += 1;
		cout << "new pos = " << this->position.first << "," << this->position.second << endl;
		if (this->position.second > 10)
		{
			this->position.second = 10;
		}
		break;
	case 3://south
		cout << "previous pos = " << this->position.first << "," << this->position.second << endl;
		this->position.first += 1;
		cout << "new pos = " << this->position.first << "," << this->position.second << endl;
		if (this->position.first > 10)
		{
			this->position.first = 10;
		}
		break;
	case 4://west
		cout << "previous pos = " << this->position.first << "," << this->position.second << endl;
		this->position.first -= 1;
		cout << "new pos = " << this->position.first << "," << this->position.second << endl;
		if (this->position.second < 0)
		{
			this->position.second = 0;
		}
		break;
	}*/
}

void Bug::randomDirection()
{
	//cout << this->id << " is stuck at " << this->position.first << "," << this->position.second << " facing " << this->direction << endl;
	//cout << "old direction = " << this->direction<<endl;
	this->direction = rand() % 4 + 1;
	//cout << "new direction = " << this->direction << endl;

}

bool Bug::isWayBlocked()
{
	switch (this->direction)
	{
	case 1://north
		if (this->position.second == 0)
		{
			return true;
		}
		else { return false; }
		break;
	case 2://east
		if (this->position.first == 10)
		{
			return true;
		}
		else { return false; }
		break;
	case 3://south
		if (this->position.second == 10)
		{
			return true;
		}
		else { return false; }
		break;
	case 4://west
		if (this->position.first == 0)
		{
			return true;
		}
		else { return false; }
		break;
	}
}


bool Bug::isAlive()
{
	return this->alive;
}

bool Bug::killed()
{
	this->alive = false;
	return true;
}

int Bug::getSize()
{
	return this->size;
}

pair<int, int> Bug::getPos()
{
	return this->position;
}

list<pair<int, int>> Bug::getPath()
{
	return this->path;
}

int Bug::getId()
{
	return this->id;
}


ostream& operator<<(ostream& strm, const Bug& bug)
{
	return strm << "Id: " << bug.id << " Position: (" << bug.position.first << "," << bug.position.second << ") Direction: " << bug.direction << " Size: " << bug.size << " Alive: " << bug.alive << endl;
}
