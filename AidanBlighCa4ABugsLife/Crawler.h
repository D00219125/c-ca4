#pragma once
#include "Bug.h"

class Crawler : public Bug
{
public:
	//I can use the default move fort the crawler
	virtual void move();
	Crawler(int id, pair<int, int> position, int direction, int size);
	friend ostream& operator<<(ostream& strm, const Crawler& craw);
};